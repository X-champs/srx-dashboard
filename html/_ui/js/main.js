jQuery(document).ready(function($){

    // Left Tabs
    $('.vehicle-left-info ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
        $('.vehicle-left-info ul.tabs li').removeClass('current');
        $('.vehicle-left-info .tab-content').removeClass('current');
        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

    // Right Tabs    
    $('.vehicle-right-info .tabs-head ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
        $('.vehicle-right-info .tabs-head ul.tabs li').removeClass('current');
        $('.vehicle-right-info > .tabs-body > .tab-content').removeClass('current');
        $(this).addClass('current');
        $("#"+tab_id).addClass('current'); 
    })

    // Vehicle Tabs    
    $('.vehicle-tabs-head ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
        $('.vehicle-tabs-head ul.tabs li').removeClass('current');
        $('.vehicle-tabs-body .tab-content').removeClass('current');
        $(this).addClass('current'); 
        $("#"+tab_id).addClass('current');
    })

    // Relation b/w inventory and payments
    $('.compare-data-body li.vim').click(function(){
        $('.vehicle-right-info ul.tabs li, .vehicle-right-info .tabs-body > .tab-content').removeClass('current');
        $('.vehicle-right-info ul.tabs li.rel-tab, .vehicle-right-info .tabs-body > .tab-content.rel-tabdata').addClass('current');
        $('.default-payments').hide();
        $('.inventory-payments').show();
    })
    $('.vehicle-right-info ul.tabs li').click(function(){
        $('.default-payments').show();
        $('.inventory-payments').hide();
    })
    $('.inpayment-results table tr td.rate1').click(function(){
        $('.inpayment-results table tr td.rate1').removeClass('active');
        $(this).addClass('active'); 
        $('.vehicle-payment-one').fadeOut();
        $('.vehicle-payment-one').fadeIn();
    })



    // Accordians
    $('.verticle-tab').find('.vehicle-data').click(function(){
        $(this).toggleClass('active');
        $(this).next().slideToggle('slow');
    }); 
    $('.todo-list').find('.todo-head').click(function(){
        $(this).toggleClass('active');
        $(this).next().slideToggle('slow');
    }); 
    $('.activ-body').find('.activ-data-head').click(function(){
        $(this).toggleClass('active');
        //$('.activ-data-body').slideUp();
        $(this).next().slideToggle('slow');
    }); 

    // Deals FadeOut
    $('.deal-close').click(function() {
      $(this).parent('.deal').fadeToggle();
    });
    $('.bell-hover-data li img').click(function() {
      $(this).parent('.bell-hover-data li').fadeToggle();
    });

    // Notifications Popup
    $('.bell .bell-img').click(function() {
      $('.bell-hover').fadeIn();
    });
    $('.bell-close').click(function() {
      $('.bell-hover').fadeOut();
    });

    // Rating Popup
    $('.symbol').click(function() {
      $('.rating-block').fadeIn();
    });
    $('.rating-close').click(function() {
      $('.rating-block').fadeOut();
    });

    // Todo Popup
    $('li.to-do span').click(function() {
      $('.todo-popup').fadeIn();
    });
    $('.todo-close').click(function() {
      $('.todo-popup').fadeOut();
    });

    // Notes Popup
    $('li.notes').click(function() {
      $('.notes-popup').fadeIn();
    });
    $('.notes-close').click(function() {
      $('.notes-popup').fadeOut();
    });

    // Log Call Popup
    $('li.log-call').click(function() {
      $('.logcall-popup').fadeIn();
    });
    $('.logcall-close').click(function() {
      $('.logcall-popup').fadeOut();
    });


    //Multiple Select DropDown
    $(".incentive-dropdown a").on('click', function() {
      $(".mutliSelect ul").slideToggle('slow'); 
    });
    
    function getSelectedValue(id) {
      return $("#" + id).find(".incentive-dropdown a span.value").html();
    }

    $(document).bind('click', function(e) {
      var $clicked = $(e.target);
      if (!$clicked.parents().hasClass("dropdown")) $(".mutliSelect ul").hide();
    });

    $('.mutliSelect input[type="checkbox"]').on('click', function() {
      var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
        title = $(this).val() + ",";
      if ($(this).is(':checked')) {
        var html = '<span title="' + title + '">' + title + '</span>';
        $('.multiSel').append(html);
        $(".hida").hide();
      } else {
        $('span[title="' + title + '"]').remove();
        var ret = $(".hida");
        $('.incentive-dropdown a').append(ret);
      }
    });



});
